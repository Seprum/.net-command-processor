﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using CommandProcessor.ParameterTypes;

namespace CommandProcessor
{
	internal class Command
	{
		private readonly CommandPath[] _paths;
		private readonly string _displayName;
		private readonly bool _ignoreCase;
		private readonly MethodInfo _method;
		private readonly string _usageMessage;
		private readonly CommandParameterInfo[] _parameters;

		public Command(CommandPath[] paths, string displayName, bool ignoreCase, MethodInfo method, string usageMessage)
		{
			if (paths == null)
				throw new ArgumentNullException(nameof(paths));

			if (method == null)
				throw new ArgumentNullException(nameof(method));

			if (paths.Length == 0)
				throw new ArgumentException("Command must contain at least one path", nameof(paths));

			var index = 0;
			var count = method.GetParameters().Length;

			_paths = paths;
			_displayName = displayName;
			_ignoreCase = ignoreCase;
			_method = method;
			_usageMessage = string.IsNullOrWhiteSpace(usageMessage) ? null : usageMessage;
			_parameters = _method.GetParameters()
				.Select(p =>
				{
					var type = GetParameterType(p, index++, count);
					return type == null ?
						null :
						new CommandParameterInfo(p.Name, type, p.HasDefaultValue, p.DefaultValue);
				})
				.ToArray();

			if (_parameters.Any(p => p == null))
				throw new ArgumentException("_method has parameter of unknown type", nameof(method));
		}

		private bool GetArguments(string commandText, out object[] arguments)
		{
			arguments = new object[_parameters.Length];
			for (var i = 0; i < _parameters.Length; i++)
			{
				object argument;
				if (!_parameters[i].CommandParameterType.Parse(ref commandText, out argument))
				{
					if (!_parameters[i].IsOptional)
						return false;

					arguments[i] = _parameters[i].DefaultValue;
				}
				else
				{
					arguments[i] = argument;
				}
			}

			return true;
		}

		private CommandParameterType GetParameterType(ParameterInfo parameter, int index, int count)
		{
			var attribute = parameter.GetCustomAttribute<ParameterAttribute>();

			if (attribute != null && typeof (CommandParameterType).IsAssignableFrom(attribute.Type))
				return Activator.CreateInstance(attribute.Type) as CommandParameterType;

			if (parameter.ParameterType == typeof (string))
				return index == count - 1 ? new TextType() as CommandParameterType : new WordType();

			return parameter.ParameterType == typeof(int) ? new IntegerType() : null;
		}

		private void SendUsageMessage(TextWriter output)
		{
			output.WriteLine(_usageMessage ?? $"Usage: {this}");
		}

		public bool IsPathMatched(string commandText)
		{
			return _paths.Any(path => path.Matches(commandText, _ignoreCase));
		}

		public bool Invoke(object instance, string commandText, TextWriter output)
		{
			var commandPath = _paths.Cast<CommandPath?>()
				.FirstOrDefault(path => path != null && path.Value.Matches(commandText, _ignoreCase));

			if (!_method.IsStatic && instance == null)
				throw new ArgumentNullException("instance", "Non-static command method requires instance to be invoked from.");

			if (commandPath == null)
				return false;

			var argumentsText = commandText.Substring(commandPath.Value.Length).Trim();

			object[] arguments;
			if (!GetArguments(argumentsText, out arguments))
			{
				SendUsageMessage(output);
				return true;
			}

			var result = _method.Invoke(instance, arguments);
			return result as bool? ?? true;
		}

		public override string ToString()
		{
			var displayName = _displayName ?? _paths.OrderByDescending(path => path.Length).First().FullPath;
			return _parameters.Any() ?
				$"{displayName} " + string.Join(" ", _parameters.Select(p => p.IsOptional ? $"<{p.Name}>" : $"[{p.Name}]")) :
				displayName;
		}
	}
}