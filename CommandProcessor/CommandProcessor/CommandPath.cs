﻿using System;
using System.Linq;

namespace CommandProcessor
{
	internal struct CommandPath
	{
		public string FullPath { get; }
		public int Length => FullPath.Length;

		public CommandPath(params string[] words)
		{
			if (words == null)
				throw new ArgumentNullException(nameof(words));

			var nonEmptyWords = words.SelectMany(w => w.Split(' ')).Where(w => !string.IsNullOrEmpty(w)).ToArray();

			if (words.Length == 0)
				throw new ArgumentException("Must contain at least one non-empty word", nameof(nonEmptyWords));

			var group = string.Join(" ", nonEmptyWords.Take(nonEmptyWords.Length - 1));
			var path = nonEmptyWords.Last();

			FullPath = string.IsNullOrEmpty(group) ? path : $"{group} {path}";
		}

		public bool Matches(string commandText, bool ignoreCase)
		{
			if (commandText.Length < Length || commandText.Length > Length && commandText[Length] != ' ')
				return false;

			return ignoreCase ?
				string.Equals(commandText.Substring(0, Length), FullPath, StringComparison.CurrentCultureIgnoreCase) :
				commandText.Substring(0, Length) == FullPath;
		}

		public override string ToString()
		{
			return FullPath;
		}
	}
}