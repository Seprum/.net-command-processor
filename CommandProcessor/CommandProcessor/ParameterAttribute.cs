﻿using System;

namespace CommandProcessor
{
	/// <summary>
	/// Indicates the type of the parameter.
	/// </summary>
	[AttributeUsage(AttributeTargets.Parameter)]
	public class ParameterAttribute : Attribute
	{
		public Type Type { get; }

		/// <summary>
		/// Initializes a new instance of the <see cref="ParameterAttribute" /> class.
		/// </summary>
		/// <param name="type">Parameter's type.</param>
		public ParameterAttribute(Type type)
		{
			Type = type;
		}
	}
}
