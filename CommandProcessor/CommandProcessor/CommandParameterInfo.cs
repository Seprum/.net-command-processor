﻿using System;
using CommandProcessor.ParameterTypes;

namespace CommandProcessor
{
	internal class CommandParameterInfo
	{
		public CommandParameterType CommandParameterType { get; }
		public bool IsOptional { get; }
		public string Name { get; }
		public object DefaultValue { get; }

		public CommandParameterInfo(string name, CommandParameterType commandParameterType, bool isOptional, object defaultValue)
		{
			if (name == null)
				throw new ArgumentNullException(nameof(name));

			if (commandParameterType == null)
				throw new ArgumentNullException(nameof(commandParameterType));

			CommandParameterType = commandParameterType;
			IsOptional = isOptional;
			Name = name;
			DefaultValue = defaultValue;
		}
	}
}