# **Command Processor** #
- [Introduction](#markdown-header-introduction)
- [Decorating Methods](#markdown-header-decorating-methods)
- [Paths](#markdown-header-paths)
- [Parameters](#markdown-header-parameters)
- [Custom Parameter Types](#markdown-header-custom-parameter-types)
- [Return Values](#markdown-header-return-values)
- [Grouping Commands](#markdown-header-grouping-commands)
- [Default Values](#markdown-header-default-values)
- [Overloading Commands](#markdown-header-overloading-commands)
- [Ignore Case](#markdown-header-ignore-case)
- [Usage Message](#markdown-header-usage-message)

# Introduction
Command Processor provides an easy-to-use commands system. A command can be created by registering a class to the `CommandManager` service or by decorating a method with the `Command` attribute.

**Note:** Non-static commands can be handled only when object instance is passed to `CommandManager.Process` method.

# Decorating Methods
A method can be decorated with the `Command` attribute to tell the command manager to load it. 

### Eligible Methods
- The method may be either static or non-static.
- The method may be of any protection level in a class of any accessibility level.
- The method must be marked with the `Command` attribute.
- The method's return type must be either `void` or `bool`.

```csharp
class CommandExample
{
    [Command("helloworld")]
    private static void HelloCommand(int times)
    {
        for(var i = 0; i < times; i++)
            Console.WriteLine("Hello, world!");
    }
}
```

# Paths
The path is the name of a command. For example command `say hello` has command path `say`. You can assign a command multiple paths simply by listing them all within the `Command` attribute.

The following command is available via `/helloworld [times]`, `/hello [times]` and `/h [times]`:

```csharp
class MultiplePathsExample
{
    [Command("helloworld", "hello", "h")]
    private static void HelloCommand(int times)
    {
        for(var i = 0; i < times; i++)
            Console.WriteLine("Hello, world!");
    }
}
```

### Display Name
If a command has multiple names, you can specify the display name which will be displayed in the usage message.

```csharp
[Command("helloworld", "hello", "h", DisplayName = "hello")]
```

### Shortcut
If a command is in a group, but you want to give it a short path, you can specify a shortcut. The following command is available via `/create file [name]`, `/create f [name]` and `/cf [name]` (the shortcut):

```csharp
[CommandGroup("create")]
class ShortcutExample
{
    [Command("file", "f", Shortcut = "cf")]
    private static void CreateFileCommand(string name)
    {
        ...
    }
}
```

# Parameters
The framework supports a number of parameter types by default: `int`, `double`, `string` or any type of enumeration. The command text is automatically parsed for these types.

```csharp
class ParametersExample
{
    [Command("drop")]
    private static void DropCommand(string item, int count)
    {
        Console.WriteLine("You have dropped {count} {item}(s)");
    }
}
```

The behavior of `string` parameters is a special case. If a non-last parameter of type `string` is parsed, only a *single* word (separated by a space) is taken. If a last parameter of type `string` is parsed, the whole remainder of the command text is taken.

```csharp
class StringBehaviorExample
{
    [Command("repeat")]
    private void RepeatCommand(string word, int times)
    {
        for(var i = 0; i < times; i++)
            Console.WriteLine(word);
    }

    [Command("represent")]
    private static void RepresentCommand(string item)
    {
        Console.WriteLine($"This is {item}.");
    }
}
```

# Custom Parameter Types

### Specifying Parser
If you'd like a parameter to be parsed in a non-default way you can explicitly specify the parser.

```csharp
class SpecifyParserExample
{
    [Command("print")]
    private void PrintCommand([Parameter(typeof(WordType))]string text)
    {
        // `print Hello World` still only prints `Hello` twice. `World` is discarded.

        for(var i = 0; i < 2; i++)
            Console.WriteLine(text);
    }
}
```

### Writing Parsers
It is also possible to write your own parsers.

```csharp
class CustomType : ICommandParameterType
{
    public bool Parse(ref string commandText, out object output)
    {
        output = null;

        // Can't parse without intput.
        if (string.IsNullOrWhiteSpace(commandText))
            return false;

        // Get the first word.
        var word = commandText.TrimStart().Split(' ').First();

        // Set the output (color) based on the input.
        switch (word.ToLower())
        {
            case "red":
                output = ConsoleColor.Red;
                break;
            case "green":
                output = ConsoleColor.Green;
                break;
            case "blue":
                output = ConsoleColor.Blue;
                break;
        }

        // Remove the word from the input and trim the start.
        if (output != null)
        {
            commandText = commandText.Substring(word.Length).TrimStart();
            return true;
        }

        return false;
    }
}

class CustomParameterTypeExample
{
    [Command("cprint")]
    private static void ColoredPrintCommand([Parameter(typeof(CustomType))]ConsoleColor color, string text)
    {
        // Remember previous color
        var previousColor = Console.ForegroundColor;
        
        // Change color
        Console.ForegroundColor = color;

        // Print text
        Console.WriteLine(text);

        // Reset color to previous one
        Console.ForegroundColor = previousColor;
    }
}
```

# Return Values

The command manager accepts two return types: `void` and `bool`. If the `void` return type is used then command manager assumes that the execution has successfully concluded.

```csharp
class ReturnExample
{
    [Command("useless")]
    private static bool UselessCommand()
    {
        return false;
    }
}
```

# Grouping Commands

The commands can be groupped in order to keep commands organized. This is an optional feature and can be used if desired.

### Assign Group to Class
In order to assign a command group to a class, attach a `CommandGroup` attribute to the class.

```csharp
[CommandGroup("file"))]
class FileCommands
{
    [Command("create")]
    private static void CreateCommand(string fileName)
    {
        // Command path: `file create`
    }

    [Command("delete"))]
    private static void DeleteCommand(string fileName)
    {
        // Command path: `file delete`
    }
}
```

### Nesting
Command groups can also be nested or assigned synonyms.

```csharp
[CommandGroup("storage"))]
class StorageCommands
{
    [CommandGroup("file", "f")]
    class StorageFilesCommands
    {
        [Command("create")]
        private static void CreateCommand(string fileName)
        {
            // Command: `/storage file create` or `/storage f create`
        }

        [Command("delete"))]
        private static void DeleteCommand(string fileName)
        {
            // Command paths: `storage file delete` or `/storage f delete`
        }
    }
}
```

### Assign Group to Method
Command groups can be assigned to specific methods in various ways.

```csharp
class GroupToMethodExample
{
    [Command("file create")]
    private static void FileCreateCommand(string fileName)
    {
        // Command path: `file create`
    }

    [CommandGroup("file")]
    [Command("delete")]
    private static void FileDeleteCommand(string fileName)
    {
        // Command path: `file delete`
    }
}
```

# Default Values

You can make tailing parameters optional by specifying a default value.
```csharp
class AnyClass
{
    [Command("repeat")]
    private static void RepeatCommand(string word, int times = 1)
    {
        // Command path: `repeat [word] <times>`
        // `repeat hello` prints `hello` once
        // `repeat hello 2` prints `hello` twice

        for(var i = 0; i < times; i++)
            Console.WriteLine(word);
    }
}
```

# Overloading Commands

You can specify different overloads of a single command with different arguments. 

**Be aware!** If one overload ends, for example, with a string and the other with a number, eg. `test [message]` and `test [number]`, then a call with command text `test 123` might be handled by `test [message]`!

```csharp
class CommandOverloadingExamlple
{
    [Command("create")]
    private static void CreateCommand(int amount)
    {
        Console.WriteLine($"You have created {amount} item(s).");
    }
    [Command("create")]
    private static void CreateCommand(string name, amount)
    {
        Console.WriteLine($"You have created {amount} '{name}' item(s).");
    }
}
```

# Ignore Case

By default the case of a command is ignored. Both `/help` and `/HeLP` will be handled by a command marked `[Command("help")]`. If you do not want the case to be ignored, set IgnoreCase to false.

```csharp
class IgnoreCaseExample
{
    [Command("HELP", IgnoreCase = false)]
    private static void HelpCommand()
    {
        Console.WriteLine("YOU HAVE ENTERED HELP IN UPPER CASE");
    }
}
```

# Usage Message

By default, the command manager automatically constructs a usage message if the user failed to enter a properly formatted command. You can override the default usage message by specifying the `UsageMessage` value in the `Command` attribute.

```csharp
class UsageMessageExample
{
    [Command("help", UsageMessage = "Usage: /help [commandName] [params (space-seperated)]")]
    private static void HelpCommand(string commandName, string params)
    {
        ...
    }
}
```