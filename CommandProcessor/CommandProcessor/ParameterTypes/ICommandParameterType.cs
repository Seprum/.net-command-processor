﻿namespace CommandProcessor.ParameterTypes
{
	/// <summary>
	/// Defines methods for a command parameter type.
	/// </summary>
	public interface CommandParameterType
	{
		bool Parse(ref string commandText, out object output);
	}
}