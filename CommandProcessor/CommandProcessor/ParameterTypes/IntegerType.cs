﻿namespace CommandProcessor.ParameterTypes
{
	/// <summary>
	/// Represents an integer parameter type.
	/// </summary>
	public class IntegerType : CommandParameterType
	{
		public bool Parse(ref string commandText, out object output)
		{
			var text = commandText.TrimStart();
			output = null;

			if (string.IsNullOrEmpty(text))
				return false;

			var nextSpaceIndex = text.IndexOf(' ');
			var word = nextSpaceIndex == -1 ? text : text.Substring(0, nextSpaceIndex);

			int number;

			if (!int.TryParse(word, out number))
				return false;

			commandText = text.Substring(word.Length).TrimStart(' ');
			output = number;
			return true;
		}
	}
}