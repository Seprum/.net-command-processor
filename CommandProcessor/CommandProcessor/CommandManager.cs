﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

namespace CommandProcessor
{
	/// <summary>
	/// Represents the commands processor.
	/// </summary>
	public class CommandManager
	{
		private static readonly Type[] SupportedReturnTypes = { typeof(bool), typeof(void) };
		private readonly List<Command> _commands = new List<Command>();

		/// <summary>
		/// Finds and registers commands in specified class.
		/// </summary>
		/// <typeparam name="T">Class to register commands in.</typeparam>
		public void RegisterCommands<T>() where T : class
		{
			RegisterCommands(typeof (T));
		}

		/// <summary>
		/// Finds and registers commands in specified assembly.
		/// </summary>
		/// <param name="assembly">Assembly to register commands in.</param>
		public void RegisterCommands(Assembly assembly)
		{
			foreach (var type in assembly.GetTypes()
				// Get all classes in the specified assembly.
				.Where(type => type.IsClass))
			{
				RegisterCommands(type);
			}
		}

		private void RegisterCommands(Type type)
		{
			foreach (var method in type
				// Select the methods in the type.
				.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance)
				// Exclude abstract methods.
				.Where(method => !method.IsAbstract)
				// Only include methods with a return type of bool or void.
				.Where(method => SupportedReturnTypes.Contains(method.ReturnType))
				// Only include methods which have a command attribute.
				.Where(method => method.GetCustomAttribute<CommandAttribute>() != null))
			{
				var attribute = method.GetCustomAttribute<CommandAttribute>();

				var commandPaths = attribute.Paths.Select(name => new CommandPath(name)).ToList();

				if (commandPaths.Count == 0)
					commandPaths.AddRange(attribute.Paths.Select(name => new CommandPath(name)));

				if (!string.IsNullOrWhiteSpace(attribute.Shortcut))
					commandPaths.Add(new CommandPath(attribute.Shortcut));

				Register(commandPaths.ToArray(), attribute.DisplayName, attribute.IgnoreCase,
					method, attribute.UsageMessage);
			}
		}

		/// <summary>
		/// Processes the command text for static command.
		/// </summary>
		/// <param name="commandText">Command text to process.</param>
		/// <param name="output">Output to display command usage.</param>
		/// <returns><c>true</c> if command is invoked successfully, <c>false</c> otherwise.</returns>
		public bool Process(string commandText, TextWriter output)
		{
			return Process(null, commandText, output);
		}

		/// <summary>
		/// Handles the command text for non-static command.
		/// </summary>
		/// <param name="instance">Instance in which command method is located.</param>
		/// <param name="commandText">Command text to process.</param>
		/// <param name="output">Output to display command usage.</param>
		/// <returns><c>true</c> if command is invoked successfully, <c>false</c> otherwise.</returns>
		public bool Process(object instance, string commandText, TextWriter output)
		{
			var command = GetCommandByName(commandText);
			return command != null && command.Invoke(instance, commandText, output);
		}

		private void Register(CommandPath[] commandNames, string displayName, bool ignoreCase,
			MethodInfo method, string usageMessage)
		{
			Register(CreateCommand(commandNames, displayName, ignoreCase, method,
				usageMessage));
		}

		private Command CreateCommand(CommandPath[] commandNames, string displayName, bool ignoreCase,
			MethodInfo method, string usageMessage)
		{
			return new Command(commandNames, displayName, ignoreCase, method, usageMessage);
		}

		private static IEnumerable<string> GetCommandGroupPaths(Type type)
		{
			if (type == null || type == typeof(object))
				yield break;

			var count = 0;
			var groups =
				type.GetCustomAttributes<CommandGroupAttribute>()
					.SelectMany(a => a.Paths)
					.Select(g => g.Trim())
					.Where(g => !string.IsNullOrEmpty(g)).ToArray();

			foreach (var path in GetCommandGroupPaths(type.DeclaringType))
			{
				if (groups.Length == 0)
				{
					yield return path;
				}
				else
				{
					foreach (var g in groups)
						yield return $"{path} {g}";
				}
				count++;
			}

			if (count != 0)
				yield break;

			foreach (var group in groups)
				yield return group;
		}

		private static IEnumerable<string> GetCommandGroupPaths(MethodInfo method)
		{
			var count = 0;
			var groups =
				method.GetCustomAttributes<CommandGroupAttribute>()
					.SelectMany(a => a.Paths)
					.Select(g => g.Trim())
					.Where(g => !string.IsNullOrEmpty(g)).ToArray();

			foreach (var path in GetCommandGroupPaths(method.DeclaringType))
			{
				if (groups.Length == 0)
				{
					yield return path;
				}
				else
				{
					foreach (var g in groups)
					{
						yield return $"{path} {g}";
					}
				}

				count++;
			}

			if (count != 0)
				yield break;

			foreach (var group in groups)
				yield return group;
		}

		private Command GetCommandByName(string commandText)
		{
			return _commands.FirstOrDefault(c => c.IsPathMatched(commandText));
		}

		private void Register(Command command)
		{
			if (command == null)
			{
				throw new ArgumentNullException(nameof(command));
			}

			_commands.Add(command);
		}
	}
}
