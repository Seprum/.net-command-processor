﻿namespace CommandProcessor.ParameterTypes
{
	/// <summary>
	/// Represents a text parameter type.
	/// </summary>
	public class TextType : CommandParameterType
	{
		public bool Parse(ref string commandText, out object output)
		{
			var text = commandText.Trim();

			if (string.IsNullOrEmpty(text))
			{
				output = null;
				return false;
			}

			output = text;
			commandText = string.Empty;
			return true;
		}
	}
}