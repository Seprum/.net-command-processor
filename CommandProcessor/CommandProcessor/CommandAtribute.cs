﻿using System;

namespace CommandProcessor
{
	/// <summary>
	/// Indicates that a method is a command.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method)]
	public class CommandAttribute : Attribute
	{
		public string[] Paths { get; }
		public bool IgnoreCase { get; set; }
		public string Shortcut { get; set; }
		public string DisplayName { get; set; }
		public string UsageMessage { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="CommandAttribute" /> class.
		/// </summary>
		/// <param name="path">Command's path.</param>
		public CommandAttribute(string path) : this(new[] { path }) { }

		public CommandAttribute(params string[] paths)
		{
			Paths = paths;
			IgnoreCase = true;
		}
	}
}