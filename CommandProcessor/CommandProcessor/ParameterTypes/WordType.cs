﻿namespace CommandProcessor.ParameterTypes
{
	/// <summary>
	/// Represents a word parameter type.
	/// </summary>
	public class WordType : CommandParameterType
	{
		public bool Parse(ref string commandText, out object output)
		{
			var text = commandText.TrimStart();
			
			if (string.IsNullOrEmpty(text))
			{
				output = null;
				return false;
			}

			var nextSpaceIndex = text.IndexOf(' ');
			var word = nextSpaceIndex == -1 ? text : text.Substring(0, nextSpaceIndex);

			commandText = text.Substring(word.Length).TrimStart(' ');

			output = word;
			return true;
		}
	}
}