﻿using System;

namespace CommandProcessor
{
	/// <summary>
	/// Indicates that commands within a class or a method are part of a command group.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class CommandGroupAttribute : Attribute
    {
		public string[] Paths { get; }

		/// <summary>
		/// Initializes a new instance of the <see cref="CommandGroupAttribute" /> class.
		/// </summary>
		/// <param name="paths">The relative paths of the command group.</param>
		public CommandGroupAttribute(params string[] paths)
        {
            Paths = paths;
        }
    }
}